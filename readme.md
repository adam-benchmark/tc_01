#Task from TC
Directory: tc_01
##Tworzenie klas i obiektów

1. Utworzenie interfejsu PlantInterface zawierającą metodę getType
2. Utworzenie klasy abstrakcyjnej Plant definiowanej przez interfejs PlantInterface

3. Utworzenie klas abstrakcyjnej Fruit, Vegetable rozszerzających klasę Plant
4. Utworzenie klas Apple, Orange dziedziczących po Fruit
5. Utworzenie klas Cabbage, Potato dziedziczących po Vegetable

6. Przypisanie w klasach wartości type w możliwie różny sposób.
 
7. Dodanie metody statycznej make w klasie Orange – metoda powinna zwrócić wartość „orange juice”

8. Dodanie funkcji checkObject  
```
function checkObject (Fruit $obj){
  return $obj->getType();
}
```
##Questions
###1. Różnica między public, protected, private
- public: widoczne w całym projekcie;
- pretected: widoczne w danej klasie i dziedziczących;
- private: nie widoczne poza klasą;

###2. Dlaczego brak metody getType w klasie Fruit powoduje błąd
- Stosowanie Interfejsów systematyzuje kod i wymusza użycie danej funkcjonalności;

###3. Czym jest klasa abstrakcyjna
- Znowu, chcąc usystematyzować kod, użyjemy klsay abstrakcyjnej; różni się ona tym od Interfejsów, że można tu zawrzeć oprócz definicji metody, również jej ciało; klasa abstrakcyjna nie może istenieć samodzielnie, do jej użycia potrzebna jest sytuacja, w której rozszerzymy o nią zwykłą klasę;

###4. Co trzeba zmienić w funkcji checkObject by działała poprawnie i dlaczego
- do funkcji checkObject wstrzykujemy objekt właściwego typu, typu, który chcemy zwalidowoać; toteż dla Klasy Fruit musimy walidować Fruit $type, natomiast dla klasy Vegetable musimy walidować Vegetable $type; 
###5. Jak można zastosować wyjątki (konstrukcja try cache) by zapobiec błędne wywołania funkcji checkObject (PHP 7 TypeError)
- jak? -> najlepiej skutecznie, poprzez zatrzymanie rozpropagowywania niepoprawnie działającego kodu; pokazuje to rozwiązanie w kodzie, w pliku: Web/index.php
```
 try {
            // ... $currentClass->checkObject($currentClass) ...
        } catch (TypeError $typeError) {
            //catch error here
        }
```
