<?php

namespace TC\Interfaces;

interface PlantInterface
{
    public function getType();
}