<?php

namespace TC\Interfaces;

abstract class Vegetable extends Plant
{
    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     */
    protected function setTypeInPlant($type, $listOfTypes)
    {
        $this->type = $this->validateType($type, $listOfTypes);
    }

    /**
     * @param $type
     * @param $listOfTypes
     * @return string
     */
    private function validateType($type, $listOfTypes)
    {
        return
            in_array($type, $listOfTypes) ? $type : Plant::DEFAULT_PLANT;
    }

    /**
     * @param Vegetable $obj
     * @return mixed
     */
    public function checkObject(Vegetable $obj)
    {

        return $obj->getType();
    }
}