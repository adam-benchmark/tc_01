<?php

namespace TC\Interfaces;

abstract class Fruit extends Plant
{
    /**
     * @param $type
     */
    protected function setTypeInPlant($type, $listOfTypes)
    {
        $this->type = $this->validateType($type, $listOfTypes);
    }

    /**
     * @param $type
     * @param $listOfTypes
     * @return string
     */
    private function validateType($type, $listOfTypes)
    {
        return
            in_array($type, $listOfTypes) ? $type : Plant::DEFAULT_PLANT;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Fruit $obj
     * @return mixed
     */
    public function checkObject(Fruit $obj)
    {

        return $obj->getType();
    }
}