<?php

namespace TC\Services\Vegetables;

use TC\Interfaces\Vegetable;

class Potato extends Vegetable
{
    const POTATO = 'Potato';

    const POSSIBLE_POTATO = [
        self::POTATO,
    ];

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->setTypeInPlant($type, self::POSSIBLE_POTATO);
    }

    /**
     * @return \Generator
     */
    public function getExamples()
    {
        $example[0] = 'PotatO';
        $example[1] = 'Carrot';
        $example[2] = 'Potato';

        for ($i = 0; $i < 3; $i++) {
            yield $example[$i];
        }
    }
}