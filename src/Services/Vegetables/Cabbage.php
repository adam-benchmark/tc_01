<?php

namespace TC\Services\Vegetables;

use TC\Interfaces\Vegetable;

class Cabbage extends Vegetable
{
    const CABBAGE = 'Cabbage';

    const POSSIBLE_CABBAGE = [
        self::CABBAGE,
    ];

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->setTypeInPlant($type, self::POSSIBLE_CABBAGE);
    }

    /**
     * @return \Generator
     */
    public function getExamples()
    {
        $example[0] = 'Potato';
        $example[1] = 'Carrot';
        $example[2] = 'Cabbage';

        for ($i = 0; $i < 3; $i++) {
            yield $example[$i];
        }
    }
}