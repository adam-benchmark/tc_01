<?php

namespace TC\Services\Fruits;

use TC\Interfaces\Fruit;

class Apple extends Fruit
{
    const APPLE = 'Apple';

    const POSSIBLE_APPLE = [
        self::APPLE,
    ];

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->setTypeInPlant($type, self::POSSIBLE_APPLE);
    }

    /**
     * @return \Generator
     */
    public function getExamples()
    {
        $example[0] = 'ApplE';
        $example[1] = 'Watermelon';
        $example[2] = 'Apple';

        for ($i = 0; $i < 3; $i++) {
            yield $example[$i];
        }
    }
}