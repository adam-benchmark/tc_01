<?php

namespace TC\Services\Fruits;

use TC\Interfaces\Fruit;

class Orange extends Fruit
{
    const ORANGE = 'Orange';

    const POSSIBLE_ORANGE = [
        self::ORANGE,
    ];

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->setTypeInPlant($type, self::POSSIBLE_ORANGE);
    }

    /**
     * @return \Generator
     */
    public function getExamples()
    {
        $example[0] = 'OrangeE';
        $example[1] = 'Watermelon';
        $example[2] = 'Orange';

        for ($i = 0; $i < 3; $i++) {
            yield $example[$i];
        }
    }

    /**
     * @return string
     */
    public static function getOrangeJuice()
    {
        return 'orange juice';
    }
}