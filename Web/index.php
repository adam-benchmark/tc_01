<?php

spl_autoload_register(function ($nazwa) {
    $pos = strripos($nazwa, '\\');
    $classNameWithoutNamespace = substr($nazwa, $pos + 1, strlen($nazwa));

    $paths = array(
        '' . $classNameWithoutNamespace . '.php',
        '../src/Interfaces/' . $classNameWithoutNamespace . '.php',
        '../src/Models/' . $classNameWithoutNamespace . '.php',
        '../src/Services/Fruits/' . $classNameWithoutNamespace . '.php',
        '../src/Services/Vegetables/' . $classNameWithoutNamespace . '.php',
    );
    foreach ($paths as $file) {
        if (file_exists($file)) {
            require_once($file);
        }
    }
});


echo '<p>task tc_01</p>';
echo '<hr />';
$classList = [
    'TC\Services\Fruits\Apple',
    'TC\Services\Fruits\Orange',
    'TC\Services\Vegetables\Cabbage',
    'TC\Services\Vegetables\Potato',
];
foreach ($classList as $class) {
    echo '<p>--- class ' . $class . ' feature ---</p>';
    $currentClass = new $class;

    foreach ($currentClass->getExamples() as $fruit) {
        $currentClass->setType($fruit);
        echo '<p>' . $fruit . ' type : ' . $currentClass->getType() . '</p>';
        try {
            echo '<p>' . $fruit . ' type via checkObject : ' . $currentClass->checkObject($currentClass) . '</p>';
        } catch (TypeError $typeError) {
            echo '<p>You are using wrong Type for this object : ' . $fruit . '</p>';
        }

        if ($currentClass InstanceOf TC\Services\Fruits\Orange)
            echo '<p><b>Orange with static method : ' . $currentClass::getOrangeJuice() . '</b><p>';


    }
    echo '<hr />';
}
